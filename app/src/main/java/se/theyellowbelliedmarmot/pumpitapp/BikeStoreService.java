package se.theyellowbelliedmarmot.pumpitapp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import se.theyellowbelliedmarmot.pumpitapp.model.PumpPlace;

public interface BikeStoreService {

    @GET("/maps/api/place/nearbysearch/json")
    Call<PumpPlace> getPumped(@Query("query") String query, @Query("key") String key);
//    @GET("/maps/api/place/textsearch/json")
//    Call<PumpPlace> test(@Query("keyword") String keyword, @Query("location") String location, @Query("radius") String radius, @Query("key") String key);
}
