package se.theyellowbelliedmarmot.pumpitapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.theyellowbelliedmarmot.pumpitapp.model.PumpPlace;
import se.theyellowbelliedmarmot.pumpitapp.model.Result;

public class MainActivity extends AppCompatActivity {

    private static final String DATE_KEY = "se.theyellowbelliedmarmot.pumpitapp.DATE_KEY";
    private static final String TAG = MainActivity.class.getSimpleName();

    private TextView lastVisitedDate;
    private Date date;
    private Collection<Result> pumpPlaces = new ArrayList<>();
    private Retrofit retrofit;
    public static final String BASE_URL = "https://maps.googleapis.com/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lastVisitedDate = (TextView) findViewById(R.id.last_visited_date);
        lastVisitedDate.setText(readDate());
        String lastUse = ("Latest usage: " + currentDate());
        saveDate(lastUse);

        retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

        BikeStoreService bikeStoreService = retrofit.create(BikeStoreService.class);
        Call<PumpPlace> result = bikeStoreService.getPumped("cykelbutik+stockholm+tyresö", "AIzaSyDkhr3_DxJD3-9UfKLfKJFCMcPyOpXvxN0");
//        Call<PumpPlace> result = bikeStoreService.test("cykelbutik+stockholm+tyresö", "18.1, 59.0", "50000","AIzaSyAbFq2n-WnIGmQ-JS37rjkroe-bFIAR8AI" );
        result.enqueue(new Callback<PumpPlace>() {
            @Override
            public void onResponse(Call<PumpPlace> call, Response<PumpPlace> response) {
                List<Result> results = response.body().getResults();

                for (Result r : results) {
                    pumpPlaces.add(r);
                }
            }
            @Override
            public void onFailure(Call<PumpPlace> call, Throwable t) {
                Log.d(TAG,  t.getMessage());
            }
        });
            }

    private void saveDate(String date){
        SharedPreferences preferences = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(DATE_KEY, date);
        editor.commit();
    }

    private String readDate(){
        return this.getPreferences(Context.MODE_PRIVATE).getString(DATE_KEY, getString(R.string.default_value_date_reader));
    }

    private String currentDate(){
        date = new Date();
        return date.toString();
    }

    public void getMap(View view){

        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra(getString(R.string.pumpplace_array_name), (Serializable) pumpPlaces);
        startActivity(intent);
    }

    public void getList(View view){
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.pump_recycler_view);
        Intent intent = new Intent(this, ListActivity.class);
        intent.putExtra(getString(R.string.pumpplace_array_name), (Serializable) pumpPlaces);
        startActivity(intent);
    }
}
