package se.theyellowbelliedmarmot.pumpitapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import se.theyellowbelliedmarmot.pumpitapp.model.Result;

public class ListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private List<Result> pumpPlaces;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        pumpPlaces = getIntent().getParcelableArrayListExtra(getString(R.string.pumpplace_array_name));

        recyclerView = (RecyclerView) findViewById(R.id.pump_recycler_view);
        layoutManager = new LinearLayoutManager(this);
        adapter = new PumpPlaceAdapter(this, pumpPlaces);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
