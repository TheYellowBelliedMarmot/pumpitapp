package se.theyellowbelliedmarmot.pumpitapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class PumpPlace implements Parcelable{

    private List<Result> results = new ArrayList<>();

    protected PumpPlace(Parcel in) {
        results= in.readArrayList(Result.class.getClassLoader());
    }

    public static final Creator<PumpPlace> CREATOR = new Creator<PumpPlace>() {
        @Override
        public PumpPlace createFromParcel(Parcel in) {
            return new PumpPlace(in);
        }

        @Override
        public PumpPlace[] newArray(int size) {
            return new PumpPlace[size];
        }
    };

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(results);
    }
}
