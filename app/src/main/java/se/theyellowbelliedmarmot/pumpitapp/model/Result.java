package se.theyellowbelliedmarmot.pumpitapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Result implements Parcelable{

    private String formattedAddress;
    private Geometry geometry;
    private String icon;
    private String id;
    private String name;
    private String placeId;
    private String reference;

    protected Result(Parcel in) {

        formattedAddress = in.readString();
        icon = in.readString();
        id = in.readString();
        name = in.readString();
        placeId = in.readString();
        reference = in.readString();
        geometry = in.readParcelable(Geometry.class.getClassLoader());
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(formattedAddress);
        parcel.writeString(icon);
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(placeId);
        parcel.writeString(reference);
        parcel.writeParcelable(geometry, i);
    }
}
