package se.theyellowbelliedmarmot.pumpitapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Location implements Parcelable {

    private Double lat;
    private Double lng;

    protected Location(Parcel in) {
        lat=in.readDouble();
        lng=in.readDouble();
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(lat.doubleValue());
        parcel.writeDouble(lng.doubleValue());
    }
}
