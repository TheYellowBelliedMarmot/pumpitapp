package se.theyellowbelliedmarmot.pumpitapp;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import se.theyellowbelliedmarmot.pumpitapp.model.Result;

public class PumpPlaceAdapter extends RecyclerView.Adapter<PumpPlaceAdapter.PumpPlaceViewHolder> {

    private final Context context;
    private List<Result> pumpPlaces;

    public PumpPlaceAdapter(Context context, List<Result> pumpPlaces) {
        this.context = context;
        this.pumpPlaces = pumpPlaces;
    }

    @Override
    public PumpPlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new PumpPlaceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PumpPlaceAdapter.PumpPlaceViewHolder holder, int position) {

        holder.name.setText(pumpPlaces.get(position).getName());
        holder.address.setText(pumpPlaces.get(position).getFormattedAddress());

        if(position%2 ==0){
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.myAqua));
        }else{
            holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.mylLight_gray));
        }
    }

    @Override
    public int getItemCount() {
        return pumpPlaces.size();
    }

    public static final class PumpPlaceViewHolder extends RecyclerView.ViewHolder{

        public final TextView name;
        public final TextView address;
        public PumpPlaceViewHolder(View itemView) {
            super(itemView);
            this.name= (TextView) itemView.findViewById(R.id.row_text);
            this.address= (TextView) itemView.findViewById(R.id.row2_adress);
        }
    }
}
